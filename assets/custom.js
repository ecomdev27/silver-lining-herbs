$(document).ready(function() {
  InitSlider();
  AnchorButton();
})

const InitSlider = function() {
  if($(window).width() < 769) {
    $('[data-mob-slider]').each(function() {
      $(this).slick({
        slidesToShow: $(this).attr('data-slides-to-show'),
        arrows: true,
        dots: false,
        adaptiveHeight: true,
        nextArrow: '<button class="slick-arrow slick-next"><svg xmlns="http://www.w3.org/2000/svg" width="17" height="28" viewBox="0 0 17 28" fill="none"><path d="M1.66538 25.7373L13.5341 13.8686L1.66538 1.99995" stroke="black" stroke-width="4"/></svg></button>',
        prevArrow: '<button class="slick-arrow slick-prev"><svg xmlns="http://www.w3.org/2000/svg" width="17" height="28" viewBox="0 0 17 28" fill="none"><path d="M14.8687 2L3 13.8687L14.8687 25.7374" stroke="black" stroke-width="4"/></svg></button>'
      })
    })
  }
}

const AnchorButton = function() {
  $(document).on('click', '[data-anchor-btn]', function() {
    var _class = $(this).attr('data-target');
    var $target = $('.' + _class);
    if($target.length > 0) {
      var top_x = $target.offset().top;
      $("html, body").animate({
        scrollTop: top_x
      }, 600);
    }
  })
}